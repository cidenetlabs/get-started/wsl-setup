# recordar configurar permisos en powershell como admin
# Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine
$ip=$args[0]
$port=$args[1]

"redireccionando puerto $port desde ip $ip"
netsh interface portproxy add v4tov4 listenport=$port listenaddress=0.0.0.0 connectport=$port connectaddress=$ip
