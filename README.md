# WSL Setup

Instrucciones para configurar podman (docker) en windows mediante WSL

## Getting started

1) Instalar Windows Terminal: Microsoft Store
1) Instalar WSL y debian: abrir una terminal de powershell como admin
    ```shell
    wsl --install -d debian
    ```
1) Sigue las instrucciones: Ingresar tu usuario, contraseña (para sudo) confirma contraseña.
1) Actualizar paquetes:
    ```shell
    sudo apt update && sudo apt upgrade
    ```
1) Validar version 11 como mínimo:
    ```shell
    grep "VERSION" /etc/os-release
    ```
    En caso de tener versión inferior a 11: reemplazar el contenido de [sources.list](./sources.list):
    ```shell
    sudo nano /etc/apt/sources.list
    ```
    y actualizar version:
    ```shell
    sudo apt update && sudo apt upgrade && sudo apt full-upgrade
    ```
1) (OPCIONAL) Crear enlace directo a windows:
    ```shell
    ln -s /mnt/c/Users/{user} ~/windows
    ```
1) Instalar podman:
    ```shell
    sudo apt install podman
    ```
    1) (OPCIONAL) docker-compose
        1) instalar pip3:
        ```shell
        sudo apt install python3-pip
        ```
        1) instalar podman-compose
        ```shell
        pip3 install podman-compose
        ```
1) Configurar podman: crear carpeta containers
    ```shell
    mkdir -p ~/.config/containers
    ```
1) Crear y copiar el contenido a [containers.conf](./containers.conf):
    ```shell
    nano ~/.config/containers/containers.conf
    ```
1) Crear y copiar el contenido a [registries.conf](./registries.conf):
    ```shell
    nano ~/.config/containers/registries.conf
    ```
1) Copiar configuración en [bashrc](./bashrc) (COPIAR AL FINAL DEL ARCHIVO):
    ```shell
    nano ~/.bashrc
    ```
1) Reiniciar terminal
1) Validar docker(podman): 
    ```shell
    docker run hello-world
    ```
1) Instalar VSCode (windows)
1) Instalar extensión para VSCode: Remote - WSL
1) Iniciar conexión con WSL desde VSCode

__nota: para que puedas acceder a cualquier servicio iniciado en docker/linux desde tu navegador/windows debes redirecionar los puertos hacia windows `./fport.ps1 IP-LINUX PORT`_

## Configurar git desde linux
1) Installar git:
    ```shell
    sudo apt install git
    ```
1) Generar ssh key:
    ```shell
    ssh-keygen -t ed25519 -C "nombre visible en repo"
    ```
1) Copiar key:
    ```shell
    cat ~/.ssh/{nombre llave}.pub
    ```
1) Pegarla en repo (ejemplo gitlab): https://gitlab.com/-/profile/keys  add new key

## Extras

Instrucciones opcionales para instalar / configurar utilidades comunmente requeridas

### Java
1) actualizar paquetes:
    ```shell
    sudo apt update && sudo apt upgrade
    ```

1) instalar la versión necesario (ejemplo con java 8):
    ```shell
    sudo apt install openjdk-8-jdk
    ```

3) si se tienen diferentes versiones instaladas, se puede seleccionar con la ayuda del comando:
    ```shell
    sudo update-alternatives --config java
    ```
    ingresando el indice de la versión que queremos usar

### Node (nvm)
Instalar nvm siguiendo las instrucciones oficiales [aqui](https://github.com/nvm-sh/nvm#installing-and-updating)

normalmente sería un comando como:
```shell
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
```

