# Pegar al final de .bashrc

alias docker="podman"
alias docker-compose="podman-compose"

export PATH="$PATH:$HOME/.local/bin"

# xdg runtime dir for wsl
if [[ -z "$XDG_RUNTIME_DIR" ]]; then
  export XDG_RUNTIME_DIR=/run/user/$UID
  if [[ ! -d "$XDG_RUNTIME_DIR" ]]; then
    export XDG_RUNTIME_DIR=/tmp/$USER-runtime
    if [[ ! -d "$XDG_RUNTIME_DIR" ]]; then
      mkdir -m 0700 "$XDG_RUNTIME_DIR"
    fi
  fi
fi

# start ssh-agent
eval $(ssh-agent -s) > /dev/null 2>&1
